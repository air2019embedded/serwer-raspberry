![grafika1](./grafika/1.png)

# Serwer
Celem projektu było uruchomienie serwera HTTP umożliwiającego zdalne sterowanie z poziomu pzeglądarki internetowej nakładką Sense Hat na Raspoberry PI. Projekt powstał w celu zaliczenia przedmiotu PUE.

## Spis treści

* [Technologie](#technologie)
* [Wprowadzenie](#wprowadzenie)
* [Instalacja Apache 2 & PHP](#instalacja)
* [Uprawnienia](#uprawnienia)

## I Technologie

* HTML 4.01
* CSS
* JavaScript
* PHP 7
* Python 2.7

## II Wprowadzenie

### Raspberry PI

Na Raspoberry PI powstały trzy programy, które zostały napisane w języku python. Wszystkie wykorzystują API dostępne na stronie [pythonhosted](https://pythonhosted.org/sense-hat/api/). Realizują następujące funkcje:

  - zapalenie dowolnie wybranej diody z panelu LED,
  - wyświetlenie tekstu na panelu LED,
  - obsługa czujników.
  
#### Zapalenie diody

Zapalanie dowolnie wybranej diody odbywa się poprzez podanie odpowiednich wartości współrzędnych X, Y oraz koloru RGB. Program wywoływany jest z przekazanymi przez nas parametrami o kolorze i lokalizacji diody. W sytuacji, gdy wywołujemy program bez żadnego argumentu następuje zgaszenie wszystkich diod. 

Wywołanie z poziomu konsoli:

```
python <scieżka do pliku> x y r g b
```
gdzie argumenty:

* *x, y* 		- odpowiadają za współrzędne diody,
* *r, g, b* 	- odpowiadają za kolor.

#### Wyświetlanie tekstu

Wyświetlenie wiadomości odbywa się poprzez podanie jej zawartości wraz z kolorem tekstu (RGB) oraz kolorem tła (RGB). Wiadomość może zostać wyświetlona jednokrotnie lub być odtwarzana w pętli w zależności od zawartości pliku loop.txt. Wiadomość będzie się wyświetlała w pętli tak długo dopóki plik będzie zawierał wartość 1.

Wywołanie z poziomu konsoli:

```
python <ścieżka do pliku> '<treść wiadomości>' tr tg tb br bg bb
```
gdzie argumenty: 

* *tr, tg, tb* - odpowiadają za kolor wiadomości [R G B],
* *br, bg, bb* - odpowiadają za kolor tła [R G B].

#### Obsługa czujników

W zależności od wartości przekazanego parametru otrzymamy informację o orientacji RPI w przestrzeni, aktualne ciśnienie, na podstawie którego obliczana jest wysokość oraz temperaturę z wilgotnością.

Wywołanie z poziomu konsoli:

```
python <ścieżka do pliku> operacja
```
W miejsce argumentu *operacja* należy wpisać jedną z trzech dostępnych opcji: "Polozenie", "Cisnienie", "Temperatura"

### Serwer Apache 2

Po stronie serwera wykonywany jest skrypt PHP, za pośrednictwem którego uruchamiane są programy sterujące bezpośrednio RPI. W momencie, gdy skrypt otrzymuje zapytanie ze strony WWW, wywołuje z poziomu konsoli odpowiedni plik pythona wraz z niezbędnymi argumentami. Wszystkie dane przekazywane są metodą POST. W przypadku pośredniczenia w obsłudze czujników skrypt oczekuje na wykonanie pliku odpowiedzialnego za ich bezpośrednią obsługe, a następnie odczytuje dane z konsoli i przekazuje je do strony internetowej.

### Strona WWW (HTML, CSS, JS)

![grafika2](./grafika/2.png)

Po stronie przeglądarki internetowej użytkownika JavaScript jest odpowiedzialny za wywoływanie skryptu PHP oraz przetwarzanie informacji do odpowiedniej formy zanim zostaną wyświetlone na stronie WWW. Dodatkowo skrypt aktualizuje zmienne w arkuszu CSS, dzięki czemu w sposób dynamiczny wyświetlany jest kolor wybrany przez użytkownika.

## III Instalacja Apache 2 & PHP

Aby zainstalować serwer oraz moduł do obsługi PHP należy podążać zgodnie z [intrukcjami](https://www.raspberrypi.org/documentation/remote-access/web-server/apache.md).

Jeżeli w przeglądarce wyświetli nam się kod PHP należy skorzystać z polecenia:

```
sudo service apache2 restart
```

## IV Uprawnienia

Do prawidłowej pracy niezbędne jest nadanie odpowiednich praw dostępu do określonych plików. W przeciwnym wypadku serwer nie będzie miał możliwości sterować nakładką Sense Hat. Pliki, o których mowa znajdują się w folderze *dev*.

Do obsługi czujników niezbędny jest dostęp do magistrali I2C
```
sudo chmod 601 /dev/i2c
```
Dostęp do folderu *fb1* umożliwia sterowanie panelem LED
```
sudo chmod 601 /dev/fb1
```
Powyższe komendy można wpisać do */etc/rc.local* przez co nie będzie konieczne ponowne nadawanie uprawnień w przypadku zaniku zasilania.